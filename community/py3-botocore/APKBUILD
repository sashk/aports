# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-botocore
# Verify required version from py3-boto3 on this package before upgrading
pkgver=1.35.6
pkgrel=0
pkgdesc="The low-level, core functionality of Boto3"
url="https://github.com/boto/botocore"
arch="noarch"
license="Apache-2.0"
depends="
	py3-certifi
	py3-dateutil
	py3-jmespath
	py3-urllib3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/botocore/botocore-$pkgver.tar.gz"
builddir="$srcdir/botocore-$pkgver"
options="!check" # take way too long

replaces=py-botocore # Backwards compatibility
provides=py-botocore=$pkgver-r$pkgrel # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
16299f6f58471d8e625dc05c86b5156d40ed6dc4d3e61c019b4f6dd41cc6227e06e85530f8071ac3af9b32ce2dae01c4e3f3326ee216d644d7e64b345fd82da9  botocore-1.35.6.tar.gz
"
