# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kpublictransport
pkgver=24.08.0
pkgrel=0
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/libraries/kpublictransport"
pkgdesc="Library to assist with accessing public transport timetables and other data"
license="BSD-3-Clause AND LGPL-2.0-or-later AND MIT"
depends="tzdata"
depends_dev="
	ki18n-dev
	networkmanager-qt-dev
	protobuf-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/libraries/kpublictransport.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpublictransport-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E "(hafasmgaterequest|efarequest)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7b05e717132a236ec53951fc27be8ad07e8c3bf74c78ef7e30d14f10ff08efb2d4c9a62549b52398a84a8d92f17efa14e19b4ce13b27842b46ade413aa130c56  kpublictransport-24.08.0.tar.xz
"
