# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kblocks
pkgver=24.08.0
pkgrel=0
pkgdesc="The classic falling blocks game"
url="https://kde.org/applications/games/kblocks/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/games/kblocks.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kblocks-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b60709ba5b3a0adef2fd0daf06f1c41c8efb4adff4a1cd26184aa16139123d2f9b3ac612aa60cd86123fa911028583ed1f8f8db8c4c09d0a393ff0bc8dcb28d4  kblocks-24.08.0.tar.xz
"
