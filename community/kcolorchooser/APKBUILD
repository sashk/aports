# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcolorchooser
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/org.kde.kcolorchooser"
pkgdesc="A color palette tool, used to mix colors and create custom color palettes"
license="MIT"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/graphics/kcolorchooser.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcolorchooser-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a4cbdde0949f664667ee2e4dbdc9b3d69f14ace0cbaab24818ce8fb7b6b852589f3a755ae962ed2fdf27a3477d6c21f37795557299e57e5709db358448b04bf8  kcolorchooser-24.08.0.tar.xz
"
