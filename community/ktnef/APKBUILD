# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktnef
pkgver=24.08.0
pkgrel=0
pkgdesc="API for handling TNEF data"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcalutils-dev
	kcontacts-dev
	ki18n-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/ktnef.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktnef-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
2c47572f7216a85bcc3a31bcae90d7998aab3333a1f1f7bd6b18ee1eb2de862ccac0745082f3a66fab78f0740b9ef20abbd72161c3a8b590a9f728d3d156427d  ktnef-24.08.0.tar.xz
"
