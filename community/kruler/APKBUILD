# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kruler
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/org.kde.kruler"
pkgdesc="An on-screen ruler for measuring pixels"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kstatusnotifieritem-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages=" $pkgname-lang"
_repo_url="https://invent.kde.org/graphics/kruler.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kruler-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6f81e41da610e6f448bfe99379f79c2e63e7860469f0edce29350e9a0e0e5751df459b6ff7957a352932f0beb75e618bc7abe6444d31c3dcb76370d113db66e5  kruler-24.08.0.tar.xz
"
