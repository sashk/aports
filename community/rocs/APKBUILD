# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=rocs
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/education/org.kde.rocs"
pkgdesc="Graph Theory IDE"
license="GPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	boost-dev
	extra-cmake-modules
	grantlee-dev
	karchive5-dev
	kconfig5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdeclarative5-dev
	kdoctools5-dev
	ki18n5-dev
	kitemviews5-dev
	ktexteditor5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
_repo_url="https://invent.kde.org/education/rocs.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/rocs-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# TestTgfFileFormat, TestRocs1FileFormat, TestRocs2FileFormat, and
	# graphtheory-test_graphoperations are broken
	# TestProject requires OpenGL
	local skipped_tests="("
	local tests="
		TestTgfFileFormat
		TestRocs1FileFormat
		TestRocs2FileFormat
		graphtheory-test_graphoperations
		TestProject"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
80cfe25611aee9ab40d9390a66f7e2debd0bee6e9d2c26e59526c1ea5b0f62476dd95419f608a5741d6770f66bad843aa8b5a4c430aeb9ffa22d8c2134b9519b  rocs-24.08.0.tar.xz
"
