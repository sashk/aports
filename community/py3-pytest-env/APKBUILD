# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-pytest-env
_pyname=pytest_env
pkgver=1.1.4
pkgrel=0
pkgdesc="Pytest plugin for adding environment variables"
url="https://github.com/MobileDynasty/pytest-env"
arch="noarch"
license="MIT"
depends="python3 py3-pytest"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs py3-installer"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/p/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}
sha512sums="
9cde819b4f68f1a19de58c86651427c608b160bdfb6b6e12c6f4b90f195a759573e442d92df08f70b807282d4b47a7c078fa0b4674433d8778a9b8fb5552209e  pytest_env-1.1.4.tar.gz
"
