# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontrast
pkgver=24.08.0
pkgrel=0
pkgdesc="Tool to check contrast for colors that allows verifying that your colors are correctly accessible"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/accessibility/kontrast"
license="GPL-3.0-or-later AND CC0-1.0"
depends="kirigami"
makedepends="
	extra-cmake-modules
	futuresql-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/accessibility/kontrast.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontrast-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
427d7bf02dd7ff81a98147610c73663a86fe357ab144015c7bfcd51e1995ce8709490d150b9f3fd7a3706f05ae4117249648c67ac7ef2ad87afedc96eb092122  kontrast-24.08.0.tar.xz
"
