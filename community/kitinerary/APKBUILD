# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kitinerary
pkgver=24.08.0
pkgrel=0
# armhf blocked by qt6-qtdeclarative
# ppc64le FTBFS
arch="all !armhf !ppc64le"
url="https://kontact.kde.org/"
pkgdesc="Data model and extraction system for travel reservation information"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcontacts-dev
	kmime-dev
	kpkpass-dev
	libphonenumber-dev
	zxing-cpp-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	ki18n-dev
	libxml2-dev
	poppler-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	shared-mime-info
	zlib-dev
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kitinerary.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kitinerary-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# skipped tests are broken
	local skipped_tests="("
	local tests="
		datatypes
		jsonlddocument
		mergeutil
		knowledgedb
		airportdb
		extractorscriptengine
		pkpassextractor
		postprocessor
		calendarhandler
		extractor
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
16605c791f12aae420a72c4692d469fd69252ac07d48f65e2b484a37b8c5d8e8e4ef45d8e7592e0d06170c8c56f07ba89ef946f4054f1d3b9957cbceb4d0128f  kitinerary-24.08.0.tar.xz
"
