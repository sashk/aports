# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmime
pkgver=24.08.0
pkgrel=0
pkgdesc="Library for handling mail messages and newsgroup articles"
arch="all"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	ki18n-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kmime.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E "kmime-(rfc2231|dateformatter|header|message)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
026e8f090a54781a27b7d058c573517e05dff49d29dfb0e4a5b460ea6e83434eba49982ee0612b60db0d0efde520440299b5584d58eeb338caa0e9a92425af05  kmime-24.08.0.tar.xz
"
