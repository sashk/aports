# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer:
pkgname=catch2-3
pkgver=3.7.0
pkgrel=0
arch="all"
url="https://github.com/catchorg/Catch2"
pkgdesc="Modern, C++-native, header-only, test framework for unit-tests (v3)"
license="BSL-1.0"
makedepends="
	cmake
	python3
	samurai
	"
source="https://github.com/catchorg/Catch2/archive/v$pkgver/catch2-v$pkgver.tar.gz"
subpackages="$pkgname-doc"
builddir="$srcdir/Catch2-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ApprovalTests is broken https://github.com/catchorg/Catch2/issues/1780
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ApprovalTests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4f577da378c9fa7c205abc145b92f8b81a998274c2e5a08fce233cd43326a6ae30645379e2c3e32abca21c2168a298fd4690893a13d65733d8bd1e7b19fefea8  catch2-v3.7.0.tar.gz
"
