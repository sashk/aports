# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmix
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/multimedia/org.kde.kmix"
pkgdesc="A sound channel mixer and volume control"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	alsa-lib-dev
	extra-cmake-modules
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	kglobalaccel5-dev
	ki18n5-dev
	kiconthemes5-dev
	knotifications5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	libcanberra-dev
	pulseaudio-dev
	qt5-qtbase-dev
	samurai
	solid5-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/kmix.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmix-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
da2ac8833cbdbad97f8e9651a608e3f4ca85ec0a093fc039f450123ec3a05d6fea9d2437cb9970eca016f8eb2232ea53613c87ed453b1fd089dd335abf09df24  kmix-24.08.0.tar.xz
"
