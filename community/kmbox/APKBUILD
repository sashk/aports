# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmbox
pkgver=24.08.0
pkgrel=0
pkgdesc="Library for accessing mail storages in MBox format"
# armhf blocked by extra-cmake-modules
# s390x blocked by kmime
arch="all !armhf !s390x"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="kmime-dev"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/pim/kmbox.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmbox-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c46ab1c3331616c2ca924f09dd5ee69758b74d24e0d9dc0b20a8df8a9ae3dd6da0bb924dc4f184b18561f82916fdc08c78d2f97251e29dc86dcb869fee9e27eb  kmbox-24.08.0.tar.xz
"
