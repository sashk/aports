# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kgeography
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kgeography"
pkgdesc="Geography Trainer"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/education/kgeography.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgeography-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9f703a662bfd4c08acf482e0d7b8a598517664d5fc575c081580702744c3b917696084cad28352f84a5f9e1ca2d6618af0d2a3ef1265a784ab56cde3cb9d9ce7  kgeography-24.08.0.tar.xz
"
