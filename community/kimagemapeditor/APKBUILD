# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kimagemapeditor
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# armv7, ppc64le, s390x, riscv64, x86 and loongarch64 blocked by qt6-qtwebengine
arch="all !armv7 !armhf !ppc64le !s390x !riscv64 !x86 !loongarch64"
url="https://kde.org/applications/development/org.kde.kimagemapeditor"
pkgdesc="An editor of image maps embedded inside HTML files, based on the <map> tag"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt6-qtbase-dev
	qt6-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/graphics/kimagemapeditor.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kimagemapeditor-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cfe33492237ee11056295c4d4bb2eaf205ea29f58a348a68ba31765d1e1219066c9048bcf2a5320ed458b05305ad9ed6cddf3bc8f4da7eba30ce8360855a6bfa  kimagemapeditor-24.08.0.tar.xz
"
