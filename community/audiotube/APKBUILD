# Contributor: Luca Weiss <luca@lucaweiss.eu>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=audiotube
pkgver=24.08.0
pkgrel=0
pkgdesc="Client for YouTube Music"
url="https://invent.kde.org/plasma-mobile/audiotube"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x, riscv64 and loongarch64 blocked by purpose -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64 !loongarch64"
license="GPL-2.0-or-later"
depends="
	gst-plugins-bad
	gst-plugins-good
	kirigami-addons
	kirigami
	purpose
	py3-ytmusicapi
	qt6-qtbase-sqlite
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	futuresql-dev
	kcrash-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev
	py3-pybind11-dev
	python3-dev
	qcoro-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/audiotube.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/audiotube-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1dc1ca0eadef3907a9d561f066573157c066e6503bbd8adccadb383bf5dfdb54adde325dd386a5d1c07da52e9f4cc5888ddc7e7e9ae839ae4f7e10dcfb865741  audiotube-24.08.0.tar.xz
"
