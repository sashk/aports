# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=khealthcertificate
pkgver=24.08.0
pkgrel=0
pkgdesc="Handling of digital vaccination, test and recovery certificates"
url="https://invent.kde.org/pim/khealthcertificate"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="LGPL-2.0-or-later"
depends_dev="
	karchive-dev
	kcodecs-dev
	ki18n-dev
	openssl-dev>3
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/pim/khealthcertificate.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/khealthcertificate-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a13a3a68dcc8aee1d243346c105c1af55d893065d49b9ce6b813f9f3b173bff7061c201c080b7c159b44309ab9decd902310199525140443df9e739f5853d062  khealthcertificate-24.08.0.tar.xz
"
