# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=create-tauri-app
pkgver=4.3.1
pkgrel=0
pkgdesc="Build tool for Leptos"
url="https://tauri.app"
# loongarch64: blocked by libc crate
arch="all !s390x !loongarch64" # nix
license="MIT OR Apache-2.0"
makedepends="cargo-auditable"
depends="cargo"
subpackages="$pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/tauri-apps/create-tauri-app/archive/refs/tags/create-tauri-app-v$pkgver.tar.gz
"
options="net"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-create-tauri-app "$pkgdir"/usr/bin/cargo-create-tauri-app

	for l in _APACHE-2.0 _MIT .spdx
	do
		install -Dm 644 LICENSE"$l" "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE"$l"
	done
}

sha512sums="
343352c1fc6d98ce6e19c29c956f3167072dc900c2ea4ae3ef8c5c376d3869f40a2d16b083cbc2ef1c867f7edc18902214f9ea029f820166e5d51654c34ecdb0  create-tauri-app-4.3.1.tar.gz
"
